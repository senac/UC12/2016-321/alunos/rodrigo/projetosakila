/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.modelo;

import java.io.Serializable;

/**
 *
 * @author Administrador
 */
public class Cidade  implements Serializable{

    private int codigo;
    private String nome;
    private Pais pais;

    public Cidade() {
    }

    public Cidade(int codigo, String nome, Pais pais) {
        this.codigo = codigo;
        this.nome = nome;
        this.pais = pais;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Pais getPais() {
        return pais;
    }

    public void setPais(Pais pais) {
        this.pais = pais;
    }

}
