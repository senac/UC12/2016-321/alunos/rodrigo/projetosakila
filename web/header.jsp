<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Sakila</title>
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    </head>
    <body>
        <! -- menu --> 
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <a class="navbar-brand" href="/ProjetoSakila/">Sakila</a>
                </div>
                <ul class="nav navbar-nav">

                    <li class="active"></li>
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Clientes
                            <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="/ProjetoSakila/cadastroCliente/cadastro.jsp">Cadastro</a></li>
                            <li><a href="/ProjetoSakila/cadastroCliente/lista.jsp">Lista</a></li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Atores
                            <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="/ProjetoSakila/cadastroAtor/cadastro.jsp">Cadastro</a></li>
                            <li><a href="/ProjetoSakila/cadastroAtor/lista.jsp">Lista</a></li>
                        </ul>
                    </li>
                    
                     <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Cidades
                            <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="/ProjetoSakila/cadastroCidade/cadastro.jsp">Cadastro</a></li>
                            <li><a href="/ProjetoSakila/cadastroCidade/lista.jsp">Lista</a></li>
                        </ul>
                    </li>
                    
                    
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Países
                            <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="/ProjetoSakila/cadastroPais/cadastro.jsp">Cadastro</a></li>
                            <li><a href="/ProjetoSakila/cadastroPais/lista.jsp">Lista</a></li>
                        </ul>
                    </li>



                    <li><a href="#">Page 1</a></li>
                    <li><a href="#">Page 2</a></li>
                    <li><a href="#">Page 3</a></li>
                </ul>
            </div>
        </nav>

