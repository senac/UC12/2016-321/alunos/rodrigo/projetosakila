<%-- 
    Document   : Lista
    Created on : 24/10/2017, 10:39:52
    Author     : Diamond
--%>

<%@page import="java.util.List"%>
<%@page import="br.com.senac.banco.CidadeDAO"%>
<%@page import="br.com.senac.modelo.Cidade"%>

<%
    CidadeDAO dao = new CidadeDAO();
    List<Cidade> lista = dao.listarTodos();
%>

<jsp:include page="../header.jsp" />

<div class="container">
    <fieldset>
        <legend>Lista de Cidades</legend>

        <table class="table table-hover">
            <thead>
                <tr>
                    <th>C�digo</th>
                    <th>Cidade</th>
                </tr>
            </thead>
            <tbody>


                <% for (Cidade c : lista) { %>

                <tr>
                    <td><% out.print(c.getCodigo()); %></td>
                    <td><% out.print(c.getNome()); %></td>
                </tr>

                <% }%>

            </tbody>
        </table>

    </fieldset>

</div>



<jsp:include page="../footer.jsp" />